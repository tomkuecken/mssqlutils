import os
import subprocess

from logging import Logger

from SqlObjectType import MssqlScripterObjectType
from MssqlScripterOptions import MssqlScripterArguments


class DatabaseBackup(object):
    def __int__(self,
                path: str):
        self.path = path


class MssqlDatabaseBackup(DatabaseBackup):
    from Database import MssqlDatabase

    # TODO: Map to multiple types
    __sub_folders = {
        'database': MssqlScripterObjectType.DATABASE,
        'tables': MssqlScripterObjectType.TABLE,
        'views': MssqlScripterObjectType.VIEW,
        'functions': MssqlScripterObjectType.USER_DEFINED_FUNCTION,
        'procedures': MssqlScripterObjectType.STORED_PROCEDURE
    }

    __default_mssqlscripter_options = [
        MssqlScripterArguments.EXCLUDE_HEADERS.value
    ]

    __path_for_other_objects = 'other'

    def __init__(self,
                 path: str,
                 database: MssqlDatabase,
                 logger: Logger):
        self.__logger = logger
        self.__database = database
        self.path = path

        # Initialize
        self.__make_dirs()

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, value):
        try:
            if not value or value.strip() == '':
                raise ValueError('Backup value cannot be empty')

            # Path must have database name in it
            path_parts = os.path.split(value)
            db = self.__database.connection.database

            if not path_parts[len(path_parts) - 1].strip().lower() == db.strip().lower():
                full_path = os.path.join(value, db)
            else:
                full_path = value

            value = full_path

            if not os.path.exists(value):
                self.__logger.info('Making path: {}'.format(value))
                try:
                    os.makedirs(value, exist_ok=True)
                except Exception as e:
                    self.__logger.critical('Unable to make backup path: {}'.format(e))
                    exit(1)
        except Exception as e:
            self.__logger.critical('Unable to create base path for the backup for given path {}: {}'.format(value, e))
            exit(1)

        self._path = os.path.abspath(value)

    def __get_subfolders(self):
        return self.__sub_folders.keys()

    def __make_dirs(self):
        """
        Make all the folders defined in __sub_folders
        This is done on initialization

        :return:
        :rtype:
        """
        for folder in self.__get_subfolders():
            path = os.path.join(self.path, folder)
            if not os.path.exists(path):
                self.__logger.info('Making path {}'.format(path))
                try:
                    os.makedirs(path, exist_ok=True)
                except Exception as e:
                    self.__logger.critical('Unable to make path {}\n{}'.format(path, e))
                    exit(1)

    def __create_path(self, path: str):
        """
        Utility function for creating parent directories if they don't exist
        :param path:
        :type path:
        :return:
        :rtype:
        """
        if not path or path.strip() == '':
            return

        if not os.path.exists(path):
            try:
                self.__logger.info('Creating path: {}'.format(path))
                os.makedirs(path, exist_ok=True)
            except Exception as e:
                self.__logger.error('Failed to create path {}\n{}'.format(path, e))

    def __get_folder_by_mssqlscript_object_type(self,
                                                object_type: MssqlScripterObjectType,
                                                is_abspath: bool = True):
        """
        Script out each type of objects to their appropriate folder
        Objects and folders defined in __sub_folders

        :param object_type: type of object (Enum)
        :type object_type:
        :param is_abspath: whether to build the absolute path when scripting the objects
        :type is_abspath:
        :return:
        :rtype: void
        """
        path = ''
        for k, v in self.__sub_folders.items():
            if MssqlScripterObjectType(v).value == object_type.value:
                path = k

        if not path:
            self.__logger.error('Unable to map MssqlScript object to path for object: {}'.format(object_type))
            return

        if is_abspath:
            return os.path.join(self.path, path)
        else:
            return path

    def __get_default_mssqlscripter_args(self):
        """
        Gets the arguments for the connection information
        This is used by all calls

        :return:
        :rtype:
        """
        return [
            'mssql-scripter',
            MssqlScripterArguments.SERVER.value,
            self.__database.connection.host,
            MssqlScripterArguments.DATABASE.value,
            self.__database.connection.database,
            MssqlScripterArguments.USER.value,
            self.__database.connection.username,
            MssqlScripterArguments.PASSWORD.value,
            self.__database.connection.password,
        ]

    def __get_mssqlscripter_args(self,
                                 is_add_default_options: bool = True,
                                 is_script_create: bool = False,
                                 is_script_drop_create: bool = False,
                                 is_file_per_object: bool = False,
                                 file_path: str = None,
                                 include_types: [] = None,
                                 exclude_types: [] = None):
        """
        Helper function to generate a list of arguments to pass to mssql-scripter

        :param is_add_default_options:
        :type is_add_default_options:
        :param is_script_create:
        :type is_script_create:
        :param is_script_drop_create:
        :type is_script_drop_create:
        :param is_file_per_object:
        :type is_file_per_object:
        :param file_path:
        :type file_path:
        :param include_types:
        :type include_types:
        :param exclude_types:
        :type exclude_types:
        :return:
        :rtype:
        """

        args = []

        if is_add_default_options:
            for opt in self.__default_mssqlscripter_options:
                args.append(opt)

        if is_script_create and not is_script_drop_create:
            args.append(MssqlScripterArguments.SCRIPT_CREATE.value)

        if is_script_drop_create:
            args.append(MssqlScripterArguments.SCRIPT_DROP_CREATE.value)
            args.append(MssqlScripterArguments.CHECK_FOR_EXISTENCE.value)

        if is_file_per_object:
            args.append(MssqlScripterArguments.FILE_PER_OBJECT.value)

        if file_path and not file_path.strip() == '':
            path = ''
            if os.path.abspath(path).strip().lower() != path.strip().lower():
                path = os.path.join(self._path, file_path)
            else:
                path = file_path

            if not os.path.exists(path):
                self.__create_path(path)

            args.append(MssqlScripterArguments.FILE_PATH.value)
            args.append(path)

        if include_types:
            try:
                args.append(MssqlScripterArguments.INCLUDE_TYPES.value)

                if type(include_types) in [tuple, list]:
                    for t in include_types:
                        args.append(t)
                elif type(include_types) == str:
                    args.append(include_types)
                else:
                    raise TypeError('Type for included types is not correct: {}'.format(type(include_types)))
            except Exception as e:
                self.__logger.error('Unable to add included types: {}'.format(e))
                if MssqlScripterArguments.INCLUDE_TYPES.value in args:
                    args.remove(MssqlScripterArguments.INCLUDE_TYPES.value)

        if exclude_types:
            try:
                args.append(MssqlScripterArguments.EXCLUDE_TYPES.value)

                if type(exclude_types) in [tuple, list]:
                    for t in exclude_types:
                        args.append(t)
                elif type(exclude_types) == str:
                    args.append(exclude_types)
                else:
                    raise TypeError('Unable to add excluded types: {}'.format(type(exclude_types)))
            except Exception as e:
                self.__logger.error('Unable to add excluded types: {}'.format(e))
                if MssqlScripterArguments.EXCLUDE_TYPES.value in args:
                    args.remove(MssqlScripterArguments.EXCLUDE_TYPES.value)

        return args

    def __do_mssqlscripter_action(self, args, is_include_default_options: bool = True):
        from platform import system as platform_system

        """
        Execute a command to mssql-scripter
        Args does not need to include connection information

        :param args:
        :type args:
        :param is_include_default_options:
        :type is_include_default_options:
        :return:
        :rtype:
        """
        try:
            if type(args) not in [tuple, list, dict]:
                raise TypeError('Arguments must type of be list, tuple, or dict')

            all_args = self.__get_default_mssqlscripter_args()

            if is_include_default_options:
                current_args = [x.strip().lower() for x in args]

                for arg in self.__default_mssqlscripter_options:
                    if arg.strip().lower() not in current_args:
                        all_args.append(arg)

            if type(args) in [tuple, list]:
                for arg in args:
                    all_args.append(arg)

            if type(args) == dict:
                for k, v in args:
                    all_args.append(k)
                    all_args.append(v)

            self.__logger.info('Executing command: {}'.format(all_args))

            if platform_system().lower() == 'windows':
                subprocess.run(all_args, shell=True)  # For Windows: shell=True
            else:
                subprocess.run(all_args)

        except Exception as e:
            self.__logger.error('Unable to perform mssql-scripter action: {}\nargs: {}'.format(e, args))

    def script_all_objects(self,
                           path: str = None,
                           is_script_drop_create: bool = True,
                           is_file_per_object: bool = True):
        args = []

        # Default is drop/create - easier to deploy from code
        if is_script_drop_create:
            args.append(MssqlScripterArguments.SCRIPT_DROP_CREATE.value)
            args.append(MssqlScripterArguments.CHECK_FOR_EXISTENCE.value)
        else:
            args.append(MssqlScripterArguments.SCRIPT_CREATE.value)

        # Do we want to create one file per object?
        if is_file_per_object:
            args.append(MssqlScripterArguments.FILE_PER_OBJECT.value)

        if not path or path.strip() == '':
            path = self._path
        else:
            self.__create_path(path)

        args.append(MssqlScripterArguments.FILE_PATH.value)
        args.append('{}'.format(path))

        self.__do_mssqlscripter_action(args)

    def __script_objects_by_type_to_path(self,
                                         object_type: MssqlScripterObjectType,
                                         is_script_drop_create: bool = True,
                                         is_file_per_object: bool = True):
        """
        Pass a specific object type (Enum) and script out those objects to the appropriate path
        Objects and their paths are defined in __sub_folders

        :param object_type:
        :type object_type:
        :param is_script_drop_create:
        :type is_script_drop_create:
        :param is_file_per_object:
        :type is_file_per_object:
        :return:
        :rtype:
        """
        path = self.__get_folder_by_mssqlscript_object_type(object_type)

        if not path:
            self.__logger.error('Unable to map path for object: {}'.format(object_type))
            return

        path = os.path.join(self.path, path)

        args = self.__get_mssqlscripter_args(is_add_default_options=True,
                                             is_script_drop_create=is_script_drop_create,
                                             is_file_per_object=is_file_per_object,
                                             file_path=path,
                                             include_types=object_type.value)
        self.__do_mssqlscripter_action(args=args, is_include_default_options=True)

    def script_other_objects(self,
                             path: str,
                             is_script_drop_create: bool = True,
                             is_file_per_object: bool = True):
        """
        This scripts any object whose type is not defined in __sub_folders

        :param path:
        :type path:
        :param is_script_drop_create:
        :type is_script_drop_create:
        :param is_file_per_object:
        :type is_file_per_object:
        :return:
        :rtype:
        """
        try:
            if not path:
                raise ValueError('Path cannot be null for scripting other objects')

            path = os.path.abspath(os.path.join(self._path, path))

            args = self.__get_mssqlscripter_args(file_path=path,
                                                 is_script_drop_create=is_script_drop_create,
                                                 is_file_per_object=is_file_per_object,
                                                 exclude_types=[v.value for k, v in self.__sub_folders.items()])
            self.__do_mssqlscripter_action(args=args, is_include_default_options=True)
        except Exception as e:
            self.__logger.error('Unable to script other objects: {}'.format(e))

    def script_objects_to_folders(self):
        """
        For each path and object type in __sub_folders,
            script them to their folder

        :return:
        :rtype:
        """
        # TODO: Be able to script multiple types
        for k, v in self.__sub_folders.items():
            self.__script_objects_by_type_to_path(object_type=v,
                                                  is_script_drop_create=True,
                                                  is_file_per_object=True)

    def do_full_backup(self):
        """
        1) Script out all object types in __sub_folders to their corresponding folder
        2) Script out all object types not defined in __sub_folders

        :return:
        :rtype:
        """
        self.script_objects_to_folders()
        self.script_other_objects(path=self.__path_for_other_objects)
