from Connection import MssqlConnection
from Database import MssqlDatabase
from DatabaseBackup import MssqlDatabaseBackup

import logging

__LOGGER = logging.getLogger(__name__)
__LOGGER.level = logging.INFO

__PATH = '/tmp/mssql/backup'

__USERNAME = 'sa'
__PASSWORD = 'dcm1*4U2U'

__DATABASES = [
    {
        'host': '172.17.0.1',
        'database': 'Northwind',
        'username': '',
        'password': ''
    }, {
        'host': '172.17.0.1',
        'database': 'pubs',
        'username': '',
        'password': ''
    }, {
        'host': '172.17.0.1',
        'database': 'AdventureWorksDW2014',
        'username': '',
        'password': ''
    }, {
        'host': '172.17.0.1',
        'database': 'AdventureWorks2014',
        'username': '',
        'password': ''
    }, {
        'host': '172.17.0.1',
        'database': 'AdventureWorks2016CTP3',
        'username': '',
        'password': ''
    }, {
        'host': '172.17.0.1',
        'database': 'WideWorldImporters',
        'username': '',
        'password': ''
    }
]


def get_connections():
    username = ''
    password = ''

    connections = []

    for database in __DATABASES:
        if not database['username']:
            username = __USERNAME
        else:
            username = database['username']

        if not database['password']:
            password = __PASSWORD
        else:
            password = database['password']

        conn = MssqlConnection(host=database['host'],
                               database=database['database'],
                               username=username,
                               password=password)

        connections.append(conn)

    return connections


def get_databases():
    databases = []

    for conn in get_connections():
        databases.append(MssqlDatabase(connection=conn))

    return databases


def get_database_backups(logger: logging.Logger = None):
    db_backups = []

    if not logger:
        logger = __LOGGER

    for db in get_databases():
        db_backups.append(MssqlDatabaseBackup(path=__PATH,
                                              database=db,
                                              logger=logger))

    return db_backups
