import Config
import logging


def get_logger():
    logger = logging.getLogger('MssqlUtils')
    logger.setLevel(logging.INFO)

    fh = logging.FileHandler('mssqlutils.log')
    fh.setLevel(logging.INFO)

    ch = logging.StreamHandler()
    ch.setLevel(logging.ERROR)

    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    fh.setFormatter(formatter)
    ch.setFormatter(formatter)

    logger.addHandler(fh)
    logger.addHandler(ch)

    return logger


def main():
    for db_backup in Config.get_database_backups(logger=get_logger()):
        db_backup.do_full_backup()


if __name__ == '__main__':
    main()
